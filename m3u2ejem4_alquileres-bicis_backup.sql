﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 31/01/2019 12:58:14
-- Server version: 5.5.5-10.1.36-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE consultaaccion4;

--
-- Drop table `alquileres`
--
DROP TABLE IF EXISTS alquileres;

--
-- Drop table `averias`
--
DROP TABLE IF EXISTS averias;

--
-- Drop table `bicis`
--
DROP TABLE IF EXISTS bicis;

--
-- Drop table `clientes`
--
DROP TABLE IF EXISTS clientes;

--
-- Set default database
--
USE consultaaccion4;

--
-- Create table `clientes`
--
CREATE TABLE clientes (
  codigo int(11) NOT NULL,
  nif varchar(12) DEFAULT NULL,
  nombre varchar(5) DEFAULT NULL,
  direccion varchar(12) DEFAULT NULL,
  telefono int(11) NOT NULL,
  poblacion varchar(12) DEFAULT NULL,
  cp int(11) NOT NULL,
  alquileres varchar(255) DEFAULT NULL,
  descuento double DEFAULT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 4096,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `cp_index` on table `clientes`
--
ALTER TABLE clientes
ADD INDEX cp_index (cp);

--
-- Create index `telefono_index` on table `clientes`
--
ALTER TABLE clientes
ADD INDEX telefono_index (telefono);

--
-- Create table `bicis`
--
CREATE TABLE bicis (
  codigo int(11) NOT NULL,
  marca varchar(5) DEFAULT NULL,
  modelo varchar(5) DEFAULT NULL,
  kms varchar(255) DEFAULT NULL,
  años varchar(255) DEFAULT NULL,
  fechaCompra datetime DEFAULT NULL,
  precio int(11) NOT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 1820,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `precio_index` on table `bicis`
--
ALTER TABLE bicis
ADD INDEX precio_index (precio);

--
-- Create table `averias`
--
CREATE TABLE averias (
  cod int(11) NOT NULL,
  bici int(11) NOT NULL,
  descripcion varchar(25) DEFAULT NULL,
  fecha datetime DEFAULT NULL,
  coste int(11) NOT NULL,
  PRIMARY KEY (cod)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 819,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `bici_index` on table `averias`
--
ALTER TABLE averias
ADD INDEX bici_index (bici);

--
-- Create index `coste_index` on table `averias`
--
ALTER TABLE averias
ADD INDEX coste_index (coste);

--
-- Create table `alquileres`
--
CREATE TABLE alquileres (
  ref int(11) NOT NULL AUTO_INCREMENT,
  cliente int(11) NOT NULL,
  bici int(11) NOT NULL,
  fecha datetime DEFAULT NULL,
  precio_total varchar(255) DEFAULT NULL,
  descuento varchar(255) DEFAULT NULL,
  kms int(11) NOT NULL,
  PRIMARY KEY (ref)
)
ENGINE = INNODB,
AUTO_INCREMENT = 12,
AVG_ROW_LENGTH = 1489,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `bici_index` on table `alquileres`
--
ALTER TABLE alquileres
ADD INDEX bici_index (bici);

--
-- Create index `cliente_index` on table `alquileres`
--
ALTER TABLE alquileres
ADD INDEX cliente_index (cliente);

--
-- Create index `kms_index` on table `alquileres`
--
ALTER TABLE alquileres
ADD INDEX kms_index (kms);

-- 
-- Dumping data for table clientes
--
INSERT INTO clientes VALUES
(1, '20202020A', 'ramon', 'vargas 1', 123123123, 'santander', 39008, NULL, 0.1),
(2, '10101010A', 'ana', 'burgos 2', 456456456, 'santander', 39784, NULL, 0.2),
(3, '45454545Q', 'luis', 'ruiz 2', 789789789, 'maliaño', 39006, NULL, 0.15),
(4, '78787878A', 'jose', 'lopez 23', 124563563, 'camargo', 39004, NULL, 0.05);

-- 
-- Dumping data for table bicis
--
INSERT INTO bicis VALUES
(1, 'orbea', 'n1', NULL, NULL, '2017-01-01 00:00:00', 5),
(2, 'klm', 'tr2', NULL, NULL, '2015-01-01 00:00:00', 8),
(3, 'asd', 'n2', NULL, NULL, '2019-01-01 00:00:00', 4),
(4, 'dde', 'n3', NULL, NULL, '2010-01-01 00:00:00', 9),
(5, 'kelme', 'n5', NULL, NULL, '2019-01-01 00:00:00', 8),
(6, 'kms', 'm2', NULL, NULL, '2019-01-01 00:00:00', 3),
(7, 'jdg', 'n1', NULL, NULL, '2018-02-01 00:00:00', 4),
(8, 'type', 'pp3', NULL, NULL, '2018-05-04 00:00:00', 5),
(9, 'lemf', 'asd', NULL, NULL, '2014-01-01 00:00:00', 5);

-- 
-- Dumping data for table averias
--
INSERT INTO averias VALUES
(1, 1, 'pinchazo', '2018-01-01 00:00:00', 12),
(2, 1, 'cubierta rota', '2018-02-01 00:00:00', 25),
(3, 1, 'pinchazo', '2018-03-01 00:00:00', 12),
(4, 1, 'pinchazo', '2018-06-02 00:00:00', 12),
(5, 1, 'pinchazo', '2018-09-09 00:00:00', 12),
(6, 2, 'cadena rota', '2018-01-01 00:00:00', 50),
(7, 2, 'pinchazo', '2018-01-05 00:00:00', 12),
(8, 3, 'cadena rota', '2019-01-20 00:00:00', 0),
(9, 9, 'pinchazo', '2015-02-01 00:00:00', 12),
(10, 9, 'cadena rota', '2016-06-04 00:00:00', 50),
(11, 9, 'pinchazo', '2018-08-08 00:00:00', 12),
(12, 4, 'cadena rota', '2011-01-01 00:00:00', 56),
(13, 4, 'pinchazo', '2012-01-01 00:00:00', 15),
(14, 4, 'cubierta rota', '2013-01-01 00:00:00', 25),
(15, 4, 'manillar', '2015-05-04 00:00:00', 45),
(16, 7, 'frenos', '2018-05-05 00:00:00', 34),
(17, 7, 'disco', '2018-06-08 00:00:00', 89),
(18, 7, 'pinchazo', '2018-10-10 00:00:00', 12),
(19, 9, 'pinchazo', '2018-10-10 00:00:00', 12),
(20, 9, 'pinchazo', '2018-12-15 00:00:00', 12);

-- 
-- Dumping data for table alquileres
--
INSERT INTO alquileres VALUES
(1, 1, 1, '2018-01-01 00:00:00', NULL, NULL, 34),
(2, 1, 2, '2018-02-02 00:00:00', NULL, NULL, 55),
(3, 2, 1, '2018-01-04 00:00:00', NULL, NULL, 67),
(4, 2, 2, '2018-02-06 00:00:00', NULL, NULL, 66),
(5, 3, 1, '2018-02-01 00:00:00', NULL, NULL, 55),
(6, 3, 1, '2018-10-10 00:00:00', NULL, NULL, 44),
(7, 4, 9, '2011-01-01 00:00:00', NULL, NULL, 3),
(8, 4, 9, '2011-01-02 00:00:00', NULL, NULL, 44),
(9, 4, 9, '2011-06-05 00:00:00', NULL, NULL, 56),
(10, 4, 9, '2014-05-08 00:00:00', NULL, NULL, 8),
(11, 4, 4, '2011-01-05 00:00:00', NULL, NULL, 9);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;