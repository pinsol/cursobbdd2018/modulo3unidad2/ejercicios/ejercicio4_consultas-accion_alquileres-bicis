﻿USE consultaaccion4;

/* Ejercicio 4 */
  -- crear FOREIGN KEYS
  ALTER TABLE alquileres
    ADD CONSTRAINT FKAlquileresClientes FOREIGN KEY(cliente)
   REFERENCES clientes(codigo) ON DELETE CASCADE ON UPDATE CASCADE;

 ALTER TABLE alquileres
    ADD CONSTRAINT FKAlquileresBicis FOREIGN KEY(Bici)
   REFERENCES bicis(codigo) ON DELETE CASCADE ON UPDATE CASCADE;

 ALTER TABLE averias
    ADD CONSTRAINT FKAveriasBicis FOREIGN KEY(bici)
   REFERENCES bicis(codigo) ON DELETE CASCADE ON UPDATE CASCADE;

/* Ejercicio 6 */
  -- a. Bicis.kms: sumar a todos los km que se hayan realizado en los distintos alquileres en las bicis
UPDATE bicis JOIN (SELECT bici, SUM(kms) totalKM FROM alquileres GROUP BY bici) c1 ON c1.bici=codigo
  SET kms=c1.totalKM;

SELECT * FROM bicis;
SELECT * FROM alquileres;

  -- b. Bicis.años: calcular los años que tiene la bici a fecha de hoy
UPDATE bicis JOIN (SELECT codigo,YEAR(NOW())-YEAR(fechaCompra) n FROM bicis GROUP BY codigo) c1 ON c1.codigo=bicis.codigo
  SET años=n;

SELECT codigo, YEAR(NOW())-YEAR(fechaCompra) FROM bicis GROUP BY codigo;
SELECT codigo,YEAR(fechaCompra) FROM bicis;
SELECT * FROM bicis;

  -- c. Bicis.averias: numero de averias de esa bici
ALTER TABLE bicis
  ADD COLUMN averias int;

UPDATE bicis JOIN (SELECT bici,COUNT(bici) n FROM averias GROUP BY bici) c1 ON c1.bici=codigo
  SET averias=n;
  
SELECT bici,COUNT(bici) n FROM averias GROUP BY bici;
SELECT * FROM averias;
SELECT * FROM bicis;

-- d. Bicis.alquileres: el numero de que se ha alquilado esa bici
ALTER TABLE bicis
  ADD COLUMN alquileres int;

UPDATE bicis JOIN (SELECT bici,COUNT(bici) n FROM alquileres GROUP BY bici) c1 ON c1.bici=codigo
  set alquileres=n;
  
SELECT bici,COUNT(bici) n FROM alquileres GROUP BY bici;
SELECT * FROM alquileres;
SELECT * FROM bicis;

-- e. Bicis.gastos: el gasto total sumando todas las averias
ALTER TABLE bicis
  ADD COLUMN gastos float;

UPDATE bicis JOIN (SELECT bici,SUM(coste) n FROM averias GROUP BY bici) c1 ON c1.bici=codigo
  set gastos=n;
  
SELECT bici,SUM(coste) n FROM averias GROUP BY bici;
SELECT * FROM averias;
SELECT * FROM bicis;

-- f. Bicis.beneficios: suma todas las ganancias de esa bici en los alquileres (sumando precios)